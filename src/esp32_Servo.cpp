// #include <ESP32Servo.h>
// Servo myservo; //ประกาศตัวแปรแทน Servo
// void setup()
// {
//   myservo.attach(13); // D22 (กำหนดขาควบคุม Servo)
// }
// void loop()
// {
//   myservo.write(0); // สั่งให้ Servo หมุนไปองศาที่ 0
//   delay(1000); // หน่วงเวลา 1000ms
//   myservo.write(90); // สั่งให้ Servo หมุนไปองศาที่ 90
//   delay(1000); // หน่วงเวลา 1000ms
//   myservo.write(180); // สั่งให้ Servo หมุนไปองศาที่ 180
//   delay(2000); // หน่วงเวลา 2000ms
// }
#include <Arduino.h>
#include <Debounce.h>
#include <ESP32Servo.h>
#include <Adafruit_PWMServoDriver.h>
#include <esp_now.h>
#include <WiFi.h>
#include <esp_wifi.h>

#define BOARD_ID 3

//MAC Address of the receiver 
// uint8_t broadcastAddress[] = {0xa4, 0xcf, 0x12, 0x8f, 0xb8, 0xac};
// uint8_t broadcastAddress[] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
uint8_t broadcastAddress[] = {0x3c, 0x61, 0x05, 0x03, 0x9b, 0x14};


byte button1 = GPIO_NUM_27;
byte button2 = GPIO_NUM_26;
byte button3 = GPIO_NUM_25;

static const int servo_1 = GPIO_NUM_13;
static const int servo_2 = GPIO_NUM_12;

// Debounce Button(button);
Debounce Button1(button1, 50, true);
Debounce Button2(button2, 50, true);
Debounce Button3(button3, 50, true);

Servo myservo1;
Servo myservo2;

int drink;

int pos = 0;   
typedef struct struct_message {
  float ultra_1;
  float ultra_2;
  int flavor_1;
  int flavor_2;
  int glass;
} struct_message;

struct_message message;
struct_message myData;

unsigned long previousMillis = 0;   // Stores last time temperature was published
const long interval = 5000;        // Interval at which to publish sensor readings

unsigned int readingId = 0;

// Insert your SSID
constexpr char WIFI_SSID[] = "PonPond";

int32_t getWiFiChannel(const char *ssid) {
  if (int32_t n = WiFi.scanNetworks()) {
      for (uint8_t i=0; i<n; i++) {
          if (!strcmp(ssid, WiFi.SSID(i).c_str())) {
              return WiFi.channel(i);
          }
      }
  }
  return 0;
}


// callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}

void data_receive(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(&message, incomingData, sizeof(message));
  Serial.print("Bytes received: ");
  Serial.println(len);
  Serial.print("ultra_1: ");
  Serial.println(message.ultra_1);
  Serial.print("ultra_2: ");
  Serial.println(message.ultra_2);
  Serial.print("flavor_1: ");
  Serial.println(message.flavor_1);
  Serial.print("flavor_2: ");
  Serial.println(message.flavor_2);
  Serial.print("glass: ");
  Serial.println(message.glass);
  Serial.println();
}

void setup() {

  Serial.begin(115200);
  WiFi.mode(WIFI_STA);

  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  esp_now_register_recv_cb(data_receive);

  //set the mode of the pins...
  pinMode(button1, INPUT_PULLUP);
  pinMode(button2, INPUT_PULLUP);
  pinMode(button3, INPUT_PULLUP);
  // pinMode(LED_BUILTIN, OUTPUT);
  // myservo1.setPeriodHertz(50);    // standard 50 hz servo
	myservo1.attach(servo_1);
  myservo1.write(0);
  myservo2.attach(servo_2);
  myservo2.write(0);


  WiFi.mode(WIFI_STA);

  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  
  esp_now_register_recv_cb(data_receive);

  int32_t channel = getWiFiChannel(WIFI_SSID);

  WiFi.printDiag(Serial); // Uncomment to verify channel number before
  esp_wifi_set_promiscuous(true);
  esp_wifi_set_channel(channel, WIFI_SECOND_CHAN_NONE);
  esp_wifi_set_promiscuous(false);
  WiFi.printDiag(Serial); // Uncomment to verify channel change after

  //Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  
  //Register peer
  esp_now_peer_info_t peerInfo;
  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
  peerInfo.encrypt = false;
  
  //Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }


}//close void setup
int k = 0;


void loop() {

  
  // if(message.flavor_1 == 1 && message.flavor_2 == 1) {
  //   Serial.println("mixed laew");
  // }

  // else if (message.flavor_1 == 1 && message.flavor_2 == 0) {
  //   Serial.println("Strawberry");
  // }

  // else if (message.flavor_1 == 0 && message.flavor_2 == 1) {
  //   Serial.println("Chocolate");
  // }
  // for (pos = 0; pos <= 180; pos += 1) { // goes from 0 degrees to 180 degrees
	// 	// in steps of 1 degree
	// 	myservo1.write(pos);    // tell servo to go to position in variable 'pos'
	// 	delay(1000);             // waits 15ms for the servo to reach the position
	// }
	// for (pos = 180; pos >= 0; pos -= 1) { // goes from 180 degrees to 0 degrees
	// 	myservo1.write(pos);    // tell servo to go to position in variable 'pos'
	// 	delay(1000);             // waits 15ms for the servo to reach the position
	// }
  
  int buttonState1 = Button1.read();
  // digitalWrite(BUILTIN_LED,buttonState1);
  // Serial.print("button1:");
  // Serial.println(buttonState1);
  // delay(100);
  int buttonState2 = Button2.read();
  // Serial.print("button2:");
  // Serial.println(buttonState2);
  // delay(100);
  int buttonState3 = Button3.read();
  // Serial.print("button3:");
  // Serial.println(buttonState3);
  // delay(100);
  // myservo1.write(90);
  // delay(500);
  // myservo2.write(90);
  // delay(500);
  // myservo1.write(0);
  // delay(500);
  // myservo2.write(0);
  // delay(500);
  if (message.glass == 0 && (buttonState1 == 1 || buttonState2 == 1 || buttonState3 == 1)) {
    // Serial.println("k = ");
    // Serial.println(k);
    if(k==0){
        Serial.println("Please put your glass here!");
        int i;
        for(i=5;i>=0;i--){
            Serial.println(i);
            delay(1000);
        }
        Serial.println("Thank you!\n");
        k++;
    }
    else {
      k=0;
      delay(2000);
    }
  }
  else if (buttonState1 == 1 && buttonState2 == 0 && buttonState3 == 0) {
    // Serial.println("k = ");
    // Serial.println(k);
    if(k==0){
        if (message.flavor_1 == 1) {
            Serial.println("Strawberry");
            myservo1.write(90);
            delay(1000);
            buttonState1 = 0;
            buttonState2 = 0;
            buttonState3 = 0;
            myservo1.write(0);
            delay(1000);
        }
        else {
            Serial.println("Sorry,Strawberry sold out");
        }
        int i;
        for(i=5;i>=0;i--){
            Serial.println(i);
            delay(1000);
        }
        Serial.println("Thank you!\n");
        k++;
    }
    else {
      k=0;
      delay(2000);
    }
  }

  else if (buttonState1 == 0 && buttonState2 == 1 && buttonState3 == 0) {
    // Serial.println("k = ");
    // Serial.println(k);
    if(k==0){
        if (message.flavor_2 == 1) {
            Serial.println("Chocolate");
            myservo2.write(90);
            delay(1000);
            myservo2.write(0);
            delay(1000);
            buttonState1 = 0;
            buttonState2 = 0;
            buttonState3 = 0;
        }
        else {
            Serial.println("Sorry,Chocolate sold out");
        }
        int i;
        for(i=5;i>=0;i--){
            Serial.println(i);
            delay(1000);
        }
        Serial.println("Thank you!\n");
        k++;
    }
    else {
      k=0;
      delay(2000);
    }
  }

  else if (buttonState1 == 0 && buttonState2 == 0 && buttonState3 == 1) {
    // Serial.println("k = ");
    // Serial.println(k);
    if(k==0){
        if (message.flavor_1 == 1 && message.flavor_2 == 1) {
            Serial.println("Mixed");
            myservo1.write(90);
            myservo2.write(90);
            delay(1000);
            myservo1.write(0);
            myservo2.write(0);
            delay(1000);
            buttonState1 = 0;
            buttonState2 = 0;
            buttonState3 = 0;
        }
        else if (message.flavor_1 == 0){
          Serial.println("Sorry,Strawberry sold out");
        }
        else if (message.flavor_2 == 0){
          Serial.println("Sorry,Chocolate sold out");
        }
        else {
            Serial.println("Sorry,ALL sold out");
        }
        int i;
        for(i=5;i>=0;i--){
            Serial.println(i);
            delay(1000);
        }
        Serial.println("Thank you!\n");
        k++;
    }
    else {
      k=0;
      delay(2000);
    }
  }

    unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    // Save the last time a new reading was published
    previousMillis = currentMillis;

    myData.ultra_1 = message.ultra_1;
    myData.ultra_2 = message.ultra_2;
    myData.flavor_1 = message.flavor_1;
    myData.flavor_2 = message.flavor_2;
    myData.glass = message.glass;

    //Send message via ESP-NOW
    esp_err_t result = esp_now_send(broadcastAddress, (uint8_t *) &myData, sizeof(myData));
    if (result == ESP_OK) {
      Serial.println("Sent with success");
    }
    else {
      Serial.println("Error sending the data");
    }
  }

}  
