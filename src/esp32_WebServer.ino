#include <esp_now.h>
#include <WiFi.h>
#include "ESPAsyncWebServer.h"
#include <Arduino_JSON.h>

const char* ssid = "PonPond";
const char* password = "12345678";

typedef struct struct_message {
  float ultra_1;
  float ultra_2;
  int flavor_1;
  int flavor_2;
  int glass;
} struct_message;

struct_message incomingReadings;

JSONVar board;

AsyncWebServer server(80);
AsyncEventSource events("/events");

// callback function that will be executed when data is received
void OnDataRecv(const uint8_t * mac_addr, const uint8_t *incomingData, int len) {
  // Copies the sender mac address to a string
  char macStr[18];
  Serial.print("Packet received from: ");
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  Serial.println(macStr);
  memcpy(&incomingReadings, incomingData, sizeof(incomingReadings));

  board["strawberry"] = incomingReadings.ultra_1;
  board["chocolate"] = incomingReadings.ultra_2;
  String jsonString = JSON.stringify(board);
  events.send(jsonString.c_str(), "new_readings", millis());

  Serial.printf("ultra_1 value: %4.2f \n", incomingReadings.ultra_1);
  Serial.printf("ultra_2 value: %4.2f \n", incomingReadings.ultra_2);
  Serial.println();
}

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <title>GuinBin</title>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Hubballi&display=swap"
      rel="stylesheet"
    /><title>GuinBin</title>
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Hubballi&display=swap"
      rel="stylesheet"
    />
  <style>
    html {
        overflow: hidden;
        background-color: #9c89b8;
        font-family: "Hubballi", cursive;
      }
      h1 {
        font-size: 50px;
      }
      h3 {
        font-size: 35px;
        margin-top: -10px;
      }
      label {
        font-size: 20px;
      }
      .container {
        display: flex;
        justify-content: space-evenly;
        align-items: center;
      }
      body {
        height: 100vh;
        display: flex;
        justify-content: center;
        align-items: center;
      }
      .card {
        text-align:center;
        margin: auto;
        padding: 10px;
        background-color: #f0e6ef;
        border-radius: 30px;
        box-shadow: rgba(240, 46, 170, 0.4) 5px 5px,
          rgba(240, 46, 170, 0.3) 10px 10px, rgba(240, 46, 170, 0.2) 15px 15px,
          rgba(240, 46, 170, 0.1) 20px 20px, rgba(240, 46, 170, 0.05) 25px 25px;
      }
      .box {
        text-align: center;
        min-width: 400px;
        min-height: 300px;
      }
      .progress {
        width: 200px;
        height: 50px;
      }
      .minibox {
        display: flex;
        align-items: center;
        justify-content: center;
        margin-top: -20px;
      }html {
        overflow: hidden;
        background-color: #9c89b8;
        font-family: "Hubballi", cursive;
      }
      h1 {
        font-size: 50px;
      }
      h3 {
        font-size: 35px;
        margin-top: -10px;
      }
      label {
        font-size: 20px;
      }
      .container {
        display: flex;
        justify-content: space-evenly;
        align-items: center;
      }
      body {
        height: 100vh;
        display: flex;
        justify-content: center;
        align-items: center;
      }
      .card {
        margin: auto;
        padding: 10px;
        background-color: #f0e6ef;
        border-radius: 30px;
        box-shadow: rgba(240, 46, 170, 0.4) 5px 5px,
          rgba(240, 46, 170, 0.3) 10px 10px, rgba(240, 46, 170, 0.2) 15px 15px,
          rgba(240, 46, 170, 0.1) 20px 20px, rgba(240, 46, 170, 0.05) 25px 25px;
      }
      .box {
        text-align: center;
        min-width: 400px;
        min-height: 300px;
      }
      .progress {
        width: 200px;
        height: 50px;
      }
      .minibox {
        display: flex;
        align-items: center;
        justify-content: center;
        margin-top: -20px;
      }
  </style>
</head>
<body>
  <div class="card">
      <img style='margin-bottom: -40px' src='https://www.img.in.th/images/e84a8f992661e0ba2d9d897fbbcb6006.png' alt='e84a8f992661e0ba2d9d897fbbcb6006.png' border='0' height='250' />
      <div class="container">
        <div class="box">
          <img
            src="https://www.moevenpick-icecream.com/sites/default/files/styles/product_detail_image/public/products/photos/strawberry.png?itok=DfuO9gQk"
            alt="Parliament Hill"
            height="200"
          />
          <h3>Strawberry</h3>
          <div class="minibox">
            <progress id="str" class="progress" value="100" max="100"></progress>
            <label for="str" style="margin-left: 10px"><span id="str-percent"></span></label>
          </div>
        </div>
        <div class="box">
          <img
            src="https://www.moevenpick-icecream.com/sites/default/files/styles/product_detail_image/public/products/photos/11133_Scoop_Classics_SwissChocolate2.png?itok=ouLDUzha"
            alt="Parliament Hill"
            height="200"
          />
          <h3>Chocolate</h3>
          <div class="minibox">
            <progress
              id="choc"
              class="progress"
              value="100"
              max="100"
            ></progress>
            <label for="choc" style="margin-left: 10px"><span id="choc-percent"></span></label>
          </div>
        </div>
      </div>
      <h1 id="restock-text" style="text-align: center; color: #ff0a54"></h1>
    </div>
<script>
if (!!window.EventSource) {
 var source = new EventSource('/events');

 source.addEventListener('open', function(e) {
  console.log("Events Connected");
 }, false);
 source.addEventListener('error', function(e) {
  if (e.target.readyState != EventSource.OPEN) {
    console.log("Events Disconnected");
  }
 }, false);

 source.addEventListener('message', function(e) {
  console.log("message", e.data);
 }, false);

 source.addEventListener('new_readings', function(e) {
  console.log("new_readings", e.data);
  var obj = JSON.parse(e.data);
  document.getElementById("str-percent").innerHTML = obj.strawberry.toFixed(1) + "%";
  document.getElementById("choc-percent").innerHTML = obj.chocolate.toFixed(1) + "%";
  document.getElementById("str").value = obj.strawberry;
  document.getElementById("choc").value = obj.chocolate;
  if (obj.strawberry < 20){
    document.getElementById("restock-text").innerHTML = "RESTOCK";
  }
  else if(obj.chocolate < 20) {
    document.getElementById("restock-text").innerHTML = "RESTOCK";
  }
  else {
    document.getElementById("restock-text").innerHTML = "";
  }
 }, false);
}
</script>
</body>
</html>)rawliteral";

void setup() {
  // Initialize Serial Monitor
  Serial.begin(115200);

  // Set the device as a Station and Soft Access Point simultaneously
  WiFi.mode(WIFI_AP_STA);

  // Set device as a Wi-Fi Station
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Setting as a Wi-Fi Station..");
  }
  Serial.print("Station IP Address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Wi-Fi Channel: ");
  Serial.println(WiFi.channel());

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_register_recv_cb(OnDataRecv);

  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/html", index_html);
  });

  events.onConnect([](AsyncEventSourceClient * client) {
    if (client->lastId()) {
      Serial.printf("Client reconnected! Last message ID that it got is: %u\n", client->lastId());
    }
    // send event with message "hello!", id current millis
    // and set reconnect delay to 1 second
    client->send("hello!", NULL, millis(), 10000);
  });
  server.addHandler(&events);
  server.begin();
}

void loop() {
  static unsigned long lastEventTime = millis();
  static const unsigned long EVENT_INTERVAL_MS = 5000;
  if ((millis() - lastEventTime) > EVENT_INTERVAL_MS) {
    events.send("ping", NULL, millis());
    lastEventTime = millis();
  }
}
