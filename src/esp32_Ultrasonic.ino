/*
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp32-esp-now-wi-fi-web-server/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*/

#include <esp_now.h>
#include <esp_wifi.h>
#include <WiFi.h>

/////////////////////////////////////////// Ultrasonic
#include <Wire.h>

const int trigPin_1 = 17;
const int echoPin_1 = 16;

const int trigPin_2 = 14;
const int echoPin_2 = 12;

//define sound speed in cm/uS
#define SOUND_SPEED 0.034
#define CM_TO_INCH 0.393701

long duration_1;
float distanceCm_1;
float distanceInch_1;

long duration_2;
float distanceCm_2;
float distanceInch_2;
///////////////////////////////////////////


/////////////////////////////////////////// OLED
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width,  in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// declare an SSD1306 display object connected to I2C
Adafruit_SSD1306 oled(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);
///////////////////////////////////////////


// Set your Board ID (ESP32 Sender #1 = BOARD_ID 1, ESP32 Sender #2 = BOARD_ID 2, etc)
#define BOARD_ID 2

//MAC Address of the receiver 
uint8_t broadcastAddress[] = {0xa4, 0xcf, 0x12, 0x8f, 0xb8, 0xac};
//uint8_t broadcastAddress[] = {0x3c, 0x61, 0x05, 0x03, 0x9b, 0x14};
//uint8_t broadcastAddress[] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};


//Structure example to send data
//Must match the receiver structure
typedef struct struct_message {
    float ultra_1;
    float ultra_2;
    int flavor_1;
    int flavor_2;
    int glass;
} struct_message;


//Must match the sender structure
typedef struct struct_message_get {
    int glass;
} struct_message_get;

struct_message_get message_get;

void data_receive(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(&message_get, incomingData, sizeof(message_get));
  Serial.print("Glass: ");
  Serial.println(message_get.glass);
}



//Create a struct_message called myData
struct_message myData;

unsigned long previousMillis = 0;   // Stores last time temperature was published
const long interval = 2500;        // Interval at which to publish sensor readings

unsigned int readingId = 0;

// Insert your SSID
constexpr char WIFI_SSID[] = "PonPond";

int32_t getWiFiChannel(const char *ssid) {
  if (int32_t n = WiFi.scanNetworks()) {
      for (uint8_t i=0; i<n; i++) {
          if (!strcmp(ssid, WiFi.SSID(i).c_str())) {
              return WiFi.channel(i);
          }
      }
  }
  return 0;
}


// callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}
 
void setup() {
  //Init Serial Monitor
  Serial.begin(115200);
  pinMode(trigPin_1, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin_1, INPUT); // Sets the echoPin as an Input

  pinMode(trigPin_2, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin_2, INPUT); // Sets the echoPin as an Input
  // Set device as a Wi-Fi Station and set channel

  // initialize OLED display with address 0x3C for 128x64
  if (!oled.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("SSD1306 allocation failed"));
    while (true);
  }
  
  WiFi.mode(WIFI_STA);

  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  
  esp_now_register_recv_cb(data_receive);

  int32_t channel = getWiFiChannel(WIFI_SSID);

  WiFi.printDiag(Serial); // Uncomment to verify channel number before
  esp_wifi_set_promiscuous(true);
  esp_wifi_set_channel(channel, WIFI_SECOND_CHAN_NONE);
  esp_wifi_set_promiscuous(false);
  WiFi.printDiag(Serial); // Uncomment to verify channel change after

  //Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  
  //Register peer
  esp_now_peer_info_t peerInfo;
  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
  peerInfo.encrypt = false;
  
  //Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }
}
 
void loop() {  
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    // Save the last time a new reading was published
    previousMillis = currentMillis;

    // Clears the trigPin
    digitalWrite(trigPin_1, LOW);
    delayMicroseconds(2);
    // Sets the trigPin on HIGH state for 10 micro seconds
    digitalWrite(trigPin_1, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin_1, LOW);
    
    // Reads the echoPin, returns the sound wave travel time in microseconds
    duration_1 = pulseIn(echoPin_1, HIGH);
    
    // Calculate the distance
    distanceCm_1 = duration_1 * SOUND_SPEED/2;
    // Convert to inches
    distanceInch_1 = distanceCm_1 * CM_TO_INCH;
    
    // Prints the distance in the Serial Monitor
    Serial.print("Distance (cm)_1: ");
    Serial.println(distanceCm_1);
//    Serial.print("Distance (inch)_1: ");
//    Serial.println(distanceInch_1);

    // Clears the trigPin
    digitalWrite(trigPin_2, LOW);
    delayMicroseconds(2);
    // Sets the trigPin on HIGH state for 10 micro seconds
    digitalWrite(trigPin_2, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin_2, LOW);
    
    // Reads the echoPin, returns the sound wave travel time in microseconds
    duration_2 = pulseIn(echoPin_2, HIGH);
    
    // Calculate the distance
    distanceCm_2 = duration_2 * SOUND_SPEED/2;
    // Convert to inches
    distanceInch_2 = distanceCm_2 * CM_TO_INCH;
    
    // Prints the distance in the Serial Monitor
    Serial.print("Distance (cm)_2: ");
    Serial.println(distanceCm_2);
//    Serial.print("Distance (inch)_2: ");
//    Serial.println(distanceInch_2);

    distanceCm_2 = (27-distanceCm_2)/27*100;
    distanceCm_1 = (27-distanceCm_1)/27*100;
    
    oled.clearDisplay();
    oled.setTextColor(WHITE);     // text color
    oled.setTextSize(2); 
    oled.setCursor(20, 05);        
    oled.println("GuinBin");
    oled.setTextSize(1); 
    oled.setCursor(0,35);        
    oled.println("Strawberry  Chocolate"); 
    oled.setCursor(15, 45);  
    oled.print(String(distanceCm_1, 2)+"%");
    oled.setCursor(80, 45);
    oled.print(String(distanceCm_2, 2)+"%");
    oled.setCursor(10, 55);
    if (distanceCm_1 < 25){
        oled.print("Sold out");
    }
    oled.setCursor(75, 55);
    if (distanceCm_2 < 25){
        oled.print("Sold out");
    }
    oled.display();  
  
    //Set values to send
    myData.ultra_1 = distanceCm_1;
//    myData.c = 2.22222;
    myData.ultra_2 = distanceCm_2;
    myData.glass = message_get.glass;
  //  myData.glass = 1;

    if (distanceCm_1 < 25){
        myData.flavor_1 = 0;
    } else {
        myData.flavor_1 = 1;
    }

    if (distanceCm_2 < 25){
        myData.flavor_2 = 0;
    } else {
        myData.flavor_2 = 1;
    }

//    Serial.println(myData.c);
//    Serial.println(myData.b);


    //Send message via ESP-NOW
    esp_err_t result = esp_now_send(broadcastAddress, (uint8_t *) &myData, sizeof(myData));
    if (result == ESP_OK) {
      Serial.println("Sent with success");
    }
    else {
      Serial.println("Error sending the data");
    }
  }
}
