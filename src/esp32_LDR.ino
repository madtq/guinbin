#include <esp_now.h>
#include <esp_wifi.h>
#include <WiFi.h>

#define BOARD_ID 1
 
uint8_t receiverBoard[] = {0x3c, 0x61, 0x05, 0x03, 0x42, 0x70}; //MAC Address of the receiver

unsigned long previousMillis = 0;                               //Stores last time temperature was published
const long interval = 5000;                                     //Interval at which to publish sensor readings

unsigned int readingId = 0;

int light,res;

typedef struct struct_message {
    int glass;
} struct_message;

struct_message myData;

constexpr char WIFI_SSID[] = "PonPond";                         // Insert your SSID

int32_t getWiFiChannel(const char *ssid) {
  if (int32_t n = WiFi.scanNetworks()) {
      for (uint8_t i=0; i<n; i++) {
          if (!strcmp(ssid, WiFi.SSID(i).c_str())) {
              return WiFi.channel(i);
          }
      }
  }
  return 0;
}

// callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}

void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);      // Set device as a Wi-Fi Station and set channel
  
  int32_t channel = getWiFiChannel(WIFI_SSID);

  WiFi.printDiag(Serial);   // Uncomment to verify channel number before
  esp_wifi_set_promiscuous(true);
  esp_wifi_set_channel(channel, WIFI_SECOND_CHAN_NONE);
  esp_wifi_set_promiscuous(false);
  WiFi.printDiag(Serial);   // Uncomment to verify channel change after

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  
  //Register peer
  esp_now_peer_info_t peerInfo;
  memcpy(peerInfo.peer_addr, receiverBoard, 6);
  peerInfo.encrypt = false;
  
  //Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }
}

void loop() {
  
  unsigned long currentMillis = millis();
  
  light = analogRead(GPIO_NUM_36);
  
  Serial.println(light);
  
  if (light == 0){
    res = 1;
  }else if (light != 0){
    res = 0;
  }

  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis; // Save the last time a new reading was published

    myData.glass = res; //Set values to send

    //Send message via ESP-NOW
    esp_err_t result = esp_now_send(receiverBoard, (uint8_t *) &myData, sizeof(myData));
    if (result == ESP_OK) {
      Serial.println("Sent with success");
    }
    else {
      Serial.println("Error sending the data");
    }
  }
  delay(100);
}
