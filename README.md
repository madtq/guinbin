## 01204322 Embedded Systems

Department of Computer Engineering Faculty of Engineering Kasetsart University

Group : GuinBin

Project Name : GuinBin ตู้กดไอติมเม็ดอัจฉริยะ

![Developer Team](document/Member.jpg "Developer Team")

Developer :

- 6210500102 Chanyanuch Oranthavornkul
- 6210500153 Madtawan Jiracharoenvongsa
- 6210500587 Apiwat Sukthawornpradit
- 6210503888 Sirapat Inchan

Hardware :

- 4 ESP32 Nucleo32 Boards
- 1 OLED
- 2 SG90 Servo Motors
- 1 LDR
- 3 Button
- 2 Ultrasonic
- Jumper wire
- Resistor

Software :

- Arduino IDE
- PlatformIO

Library list in ESP32 :

- Adafruit_GFX.h [URL : https://github.com/adafruit/Adafruit-GFX-Library]
- Adafruit_SSD1306.h [URL : https://github.com/adafruit/Adafruit_SSD1306]
- WiFi.h
- Arduino.h
- Arduino_JSON.h
- ESPAsyncWebServer.h [URL : https://github.com/me-no-dev/ESPAsyncWebServer]
- AsyncTCP [URL : https://github.com/me-no-dev/AsyncTCP]
- esp_now.h
- ESP32Servo.h
- Debounce.h

Source Code :

- Board A : esp32_LDR.ino
- Board B : esp32_Ultrasonic.ino
- Board C : esp32_Servo.cpp
- Board D : esp32_WebServer.ino

การทำงานของแต่ละบอร์ด :

- Board A : Detect แก้ว ถ้าไม่มีแก้ว ไอติมจะไม่ไหลลงมา
- Board B : แล้วแจ้งเจ้าของร้านผ่านเว็บ + หน้า Web App โชว์ปริมาณไอติมที่เหลือ
- Board C : Detect ว่าไอติมเหลือเท่าไหร่ มี OLED แสดงรสไอติม กับปริมาณไอติมที่เหลือ
- Board D : ปุ่มกดไอติม + servo

Dataflow :

A  —>  B  —>  C  —>  D

Technique :

- บอร์ด A ใช้ LDR วัดแสงว่ามีแก้วมาวางหรือยัง ถ้าค่าที่วัดน้อยหมายความว่ามีแก้วมาวางเรียบร้อย แล้วจึงส่งข้อมูลไปให้บอร์ด B โดย Protocol ที่ใช้ติดต่อสื่อสารระหว่างบอร์ดคือ ESP-NOW โดยใช้ Mac address ของ ESP32-WROOM-32 มี Struct เพื่อเก็บค่าที่ได้รับจากอุปกรณ์ และมี Error checking ว่าปลายทางได้รับข้อมูลหรือไม่ โดยแสดงผลลัพธ์การส่งใน Serial Monitor ว่า Delivery Failed หรือ Delivery Success
- บอร์ด B จะใช้ ultrasonic ในการวัดปริมาตรของไอติมในตู้กดไอติม และทำการคำนวณออกมาเป็นเปอร์เซ็น แล้วแสดงบนจอ OLED ว่ามีไอติมในตู้อีกกี่เปอร์เซ็น โดยถ้าปริมาตรไอติมต่ำกว่าเปอร์เซ็นที่กำหนด ก็จะแสดงคำว่า Sold out ในจอ OLED ต่อมาบอร์ด B จะส่งข้อมูลปริมาตรไอติมและสถานะในการกดไอติมว่าสามารถกดได้แล้วหรือยัง ไปหาบอร์ด C
- บอร์ด C จะมีปุ่มให้กดเลือกรสไอติม(ใช้เทคนิค debouncing) โดยมี 3 ปุ่มคือ รสชาติที่ 1 รสชาติที่ 2 และรสชาติที่ผสมกัน ซึ่งเมื่อกดปุ่มระบบจะทำการเช็คค่าที่ได้รับมาจากบอร์ด B ว่าตรงตามเงื่อนไขหรือยัง โดยเช็คค่าปริมาตร และสถานะในการกดไอติม ถ้าตรงตามเงื่อนไข มอเตอร์ของไอติมรสนั้นก็จะทำงาน แล้วไอติมก็จะไหลลงมาจากตู้ แล้วบอร์ด C ก็จะส่งข้อมูลปริมาตรไอติมให้กับบอร์ด D เพื่อไปแสดงผลบนหน้าเว็บ ซึ่งหน้าเว็บจะอัปเดตอัตโนมัติทุกครั้งที่บอร์ด C ส่งข้อมูลใหม่มา
- NodeMCU-32s ใช้ C++ และ Arduino IDE

Presentation Video :

https://youtu.be/mQD_DBMUxi8
